# Runit vs Supervisor

Both [Runit](http://smarden.org/runit/) and [Supervisor](http://supervisord.org/) manage processes. They differ in philosophy and implementation, yet both are used quite often. When it comes to [Docker](https://docker.io), I personally have seen people using supervisor, but not runit, although it is IMO worth consideration. Supervisor is written in Python while Runit in C, and its memory and CPU footprint is reported to be negligible.

> Why would you need process supervision in Docker and thus violate the `service-per-container` principle? For instance, if you want to deploy your application on Google's `Cloud Run` serverless platform, maybe other similar environments.

In this project you will find Docker images based on [Alpine Linux](https://www.alpinelinux.org/) with runit & supervisor, running PHP-FPM & Nginx for a typical though old-school web application.

Running both images with docker, I was watching resource consumption by runit & supervisor processes, with htop. Turned out that supervisor consumed nearly 0% CPU and 0.5% memory on my laptop with `i7 (8 cores)` and 16GB RAM. Runit is a clear winner here with both metrics being nearly 0 throughout the observation.

Another important metric when it comes to Docker, is the image size. And again, just as I expected, Runit shines: `80MB` vs `126MB`.

Later on, I also added another folder - `nginx-unit`, to this repo. Because I work mostly with PHP, and very often with Nginx, I thought it's going to be good to have another approach to the same setup compared to the previous two. [Nginx Unit](https://unit.nginx.org/) combines the well-known Nginx with application server which can be a variety of languages, including PHP. This case differs from both `supervisor` and `runit` because it does not require process management. Obviously, it leaves you less freedom: you can only choose from a set of application runtimes like `PHP` or `Go` or `Python`. Yet, if you are designing your image for web workloads, it's probably what you might want.
`Nginx-unit` image in this repo is built `FROM alpine:edge`, and is amazingly small: only 17Mb. I wanted to start from the official `php:cli-alpine` but found out that this image doesn't support `embed` SAPI which is required to build the Nginx Unit PHP module. If you choose not the bleeding-edge but the latest stable ALpine version, then you will not be able to install `php7.4` from repositories, only 7.3.24 is available ATM. Of course, you can always choose to compile PHP from source for your own image. Given you clean up after building, you will probably get an even smaller resulting image, and you'll be able to statically compile the extensions you need, for the best performance.

In conclusion, I would like to encourage those who have a good usecase for process supervision in Docker, to try runit. It follows Unix philosophy of reliable simplicity and is highly configurable.

If all you need is a decent web-server + an application server, you might want to pay attention to [Nginx Unit](https://unit.nginx.org/).